import React, { useEffect } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import Home from "./features/home/Home";
import NotFound from "./common/NotFound";
import Report from "./features/report/Report";
import Login from "./features/auth/Login";
import axios from "axios";
import Header from "./features/header/Header";
import PrivateRoute from "./common/PrivateRoute";

const App = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const checkUserIsLoggedIn = async () => {
      try {
        const data = await axios.get(
          `${process.env.REACT_APP_API_URL}/users/session`
        );

        if (data) {
          navigate("/");
        }
      } catch (error) {
        if (!error.response.data) {
          navigate("/login");
        }
      }
    };
    checkUserIsLoggedIn();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <Header />
      <Routes>
        <Route
          path="/"
          element={
            <PrivateRoute>
              <Home />
            </PrivateRoute>
          }
        />
        <Route path="/login" element={<Login />} />
        <Route
          path="/report"
          element={
            <PrivateRoute>
              <Report />
            </PrivateRoute>
          }
        />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
};

export default App;
