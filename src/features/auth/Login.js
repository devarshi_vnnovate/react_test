import axios from "axios";
import React, { useState } from "react";
import {
  Col,
  Button,
  Row,
  Container,
  Card,
  Form,
  Modal,
} from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { login } from "./authSlice";

const Login = () => {
  const [email, setEmail] = useState("test@domain.com");
  const [password, setPassword] = useState("5t73d$66t");
  const [modalShow, setModalShow] = useState(false);

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      let payload = {
        username: email,
        password,
      };
      const data = await axios.post(
        `${process.env.REACT_APP_API_URL}/users/login`,
        payload
      );
      dispatch(login(data.data));
      navigate("/");
    } catch (error) {
      console.log(error);
      setModalShow(true);
    }
  };

  return (
    <div>
      {
        <Modal
          show={modalShow}
          onHide={() => setModalShow(false)}
          size="md"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Error, Sign in failed!
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 className="mb-2">Incorrect Username or Password</h5>
            <p>Please enter correct username and password!</p>
          </Modal.Body>
        </Modal>
      }
      <Container>
        <Row className="vh-100 d-flex justify-content-center align-items-center">
          <Col md={8} lg={6} xs={12}>
            <Card className="shadow">
              <Card.Body>
                <div className="mb-3">
                  <h5 className=" mb-4">
                    Please enter your login and password!
                  </h5>
                  <div className="mb-3">
                    <Form onSubmit={handleSubmit}>
                      <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label className="text-center">
                          Email address
                        </Form.Label>
                        <Form.Control
                          type="email"
                          placeholder="Enter email"
                          value={email}
                          onChange={(e) => setEmail(e.target.value)}
                        />
                      </Form.Group>

                      <Form.Group
                        className="mb-3"
                        controlId="formBasicPassword"
                      >
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          type="password"
                          placeholder="Password"
                          value={password}
                          onChange={(e) => setPassword(e.target.value)}
                        />
                      </Form.Group>

                      <div className="d-grid">
                        <Button variant="primary" type="submit">
                          Login
                        </Button>
                      </div>
                    </Form>
                  </div>
                </div>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Login;
