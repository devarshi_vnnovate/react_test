import axios from "axios";
import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { useSelector } from "react-redux";

const Home = () => {
  const [tableData, setTableData] = useState([
    [
      "accessed",
      "acarpous",
      "accelerometer",
      "abuts",
      "accessed",
      "accelerators",
      "acaulose",
      "acciaccatura",
    ],
    [31, 68, 93, 71, 26, 92, 88, 64],
    [100, 53, 21, 86, 90, 22, 49, 30],
    [77, 54, 57, 93, 75, 44, 100, 34],
    [27, 26, 18, 72, 3, 46, 99, 95],
    [82, 62, 38, 87, 4, 90, 98, 41],
    [33, 8, 44, 35, 83, 30, 98, 22],
    [27, 87, 75, 47, 80, 42, 24, 7],
    [2, 92, 43, 14, 13, 78, 82, 51],
    [76, 10, 50, 57, 6, 29, 81, 38],
    [63, 46, 5, 76, 23, 84, 77, 73],
  ]);

  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    const fetchTableData = async () => {
      try {
        const { data } = axios.get(
          `${process.env.REACT_APP_API_URL}/app/tabledata`,
          { withCredentials: true }
        );
        if (data) setTableData(data.tableData);
      } catch (error) {
        console.log(error);
      }
    };

    //if (auth.isAuthenticated) fetchTableData();
  }, [auth.isAuthenticated]);

  return (
    <>
      <Table className="container mt-5" striped bordered hover>
        <thead>
          <tr>
            {tableData[0].map((header, index) => (
              <th key={index}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {tableData.slice(1).map((item, index) => (
            <tr key={index}>
              {item.map((i, index) => (
                <td key={index}>{i}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default Home;
