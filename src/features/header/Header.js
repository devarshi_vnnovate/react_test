import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { logout } from "../auth/authSlice";

const Header = () => {
  const [setting, setSetting] = useState({});

  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchAppSettings = async () => {
      try {
        const { data } = await axios.get(
          `${process.env.REACT_APP_API_URL}/app/settings`
        );
        setSetting(data);
      } catch (error) {
        console.log(error);
      }
    };
    fetchAppSettings();
  }, []);

  const handleLogout = async (e) => {
    e.preventDefault();
    try {
      await axios.get(`${process.env.REACT_APP_API_URL}/users/logout`);
      dispatch(logout());
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-light bg-light navbar-fixed-top">
        <Link to="/" className="navbar-brand">
          <img src={setting.logo} width="150" height="40" alt="logo" />
        </Link>

        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="collapse navbar-collapse justify-content-end"
          id="navbarNav"
        >
          <ul className="navbar-nav">
            <li className="nav-item active">
              <Link to="/" className="nav-link">
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/report" className="nav-link">
                Report
              </Link>
            </li>

            {auth.isAuthenticated && (
              <li className="nav-item">
                <Link
                  to="/login"
                  className="nav-link"
                  onClick={(e) => handleLogout(e)}
                >
                  Logout
                </Link>
              </li>
            )}
          </ul>
        </div>
      </nav>
    </>
  );
};

export default Header;
